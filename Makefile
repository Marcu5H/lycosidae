CXX = g++
CXXFLAGS = -std=c++17 -Wall -Wextra -Werror -O2
CXXLIBS = -lcurl -lboost_program_options -lboost_regex

OBJS = \
	http.o\
	scraper.o\
	ui.o

all: lycosidae $(OBJS)

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $< $(CXXLIBS)

lycosidae: $(OBJS) main.cpp
	$(CXX) $(CXXFLAGS) -o $@ main.cpp $(OBJS) $(CXXLIBS)

clean:
	$(RM) ws
	$(RM) $(OBJS)

.PHONY:	ws
