# lycosidae
A very simple web crawler with minimal features.

---

## Build and run
```
$ make
```

```
$ lycosidae --help
Options:
  -h [ --help ]             Help message
  -u [ --url ] arg          URL to start crawling from
  -t [ --timeout ] arg (=5) HTTP request timeout
```

## Dependencies
```
libcurl
boost::regex
boost::program_options
```

### Todo
- [ ] Cleaner code
- [ ] Documentation
- [ ] Multithreading
- [ ] More tool options
