#include <boost/mpl/assert.hpp>
#include <boost/program_options.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>
#include <cstdlib>
#include <curl/curl.h>
#include <iostream>

#include "scraper.hpp"
#include "ui.hpp"

namespace po = boost::program_options;

int
main(int argc, const char *argv[])
{
	struct scraped_s s;
	try {
		po::options_description desc{"Options"};
		desc.add_options()
			("help,h", "Help message")
			("url,u", po::value<std::string>(), "URL to start crawling from")
			("timeout,t", po::value<long>()->default_value(5),
			 "HTTP request timeout")
		;

		po::variables_map vm;
		po::store(po::parse_command_line (argc, argv, desc), vm);

		if (vm.count("help")) {
			std::cout << desc;
			exit(EXIT_SUCCESS);
		}
		if (vm.count("url")) {
			s.url = vm["url"].as<std::string>();
		} else {
			std::cerr << "Missing arguments!\n";
			exit(EXIT_FAILURE);
		}
		if (vm.count("timeout")) {
			s.timeout = vm["timeout"].as<long>();
		}
	} catch (const po::error &ex) {
		std::cerr << ex.what() << '\n';
		exit(EXIT_FAILURE);
	}

	Scraper scraper(s.url);
	if (!scraper.isOk()) {
		std::cerr << "Failed to initialize scraper\n";
		exit(EXIT_FAILURE);
	}

	static UI tui;
	while (scraper.scrape(&s)->http_response_code != -1) {
		std::cout << "\033[1F\n"
				  << "[ " << s.http_response_code << " ]"
				  << " " << s.url.substr(0, 40)
				  << "\n" << '(' << scraper.getURLCountCrawled() << '/'
				  << scraper.getURLCountInQue() << ')';

		tui.flush();
	}

	return EXIT_SUCCESS;
}
