#include <boost/regex/v5/regex.hpp>
#include <boost/regex/v5/regex_fwd.hpp>
#include <boost/regex/v5/regex_search.hpp>
#include <string>
#include <type_traits>
#include <boost/regex.hpp>

#include "scraper.hpp"
#include "http.hpp"

Scraper::Scraper(std::string init_url)
{
	urls.push_back(init_url);
	this->httpOk = this->client.isOk();
	this->urlRegex = boost::regex("(http|ftp|https):\\/\\/([\\w_-]+(?:(?:\\.[\\"
								  "w_-]+)+))([\\w.,@?^=%&:\\/~+#-]*[\\w@?^=%&\\"
								  "/~+#-])");
}

bool
Scraper::isOk()
{
	return this->httpOk;
}

struct scraped_s
*Scraper::scrape(struct scraped_s *s)
{
	if (urls.size() > 0) {
		s->url = urls.back();
		crawled_urls.push_back(s->url);
	} else {
		s->http_response_code = -1;
		return s;
	}

	urls.pop_back();

	this->client.get(&this->client.gr, s->url, "Mozilla/5.0 (Android 4.4;"
						 "Mobile; rv:41.0)Gecko/41.0 Firefox/41.0",
					   (void *)this->dataHandlerCallback, s->timeout);
	s->cc = this->client.gr.cc;
	s->http_response_code = this->client.gr.code;

	this->fetchURLs();

	return s;
}

void
Scraper::fetchURLs()
{
	boost::smatch what;
	std::string match;
	while (boost::regex_search(this->client.gr.resp, what, this->urlRegex)) {
		for (auto m : what) match += m;
			if (std::find(crawled_urls.begin(), crawled_urls.end(), match)
				== crawled_urls.end()) {
				urls.push_back(match);
				match.clear();
			}
		this->client.gr.resp = what.suffix().str();
	}

	this->client.gr.resp.clear();
}

long
Scraper::getURLCountInQue()
{
	return urls.size();
}

long
Scraper::getURLCountCrawled()
{
	return crawled_urls.size();
}
