#ifndef __HTTP_H
#define __HTTP_H 1

#include <string>
#include <iostream>
#include <curl/curl.h>

#define HTTPC_DEFAULT_UA ""

class HTTPClient {
private:
	CURL *curl;
public:
	struct httpGetResp {
		CURLcode cc;
		long code;
		std::string resp;
	};
	httpGetResp gr;
public:
	HTTPClient(void);
	~HTTPClient(void);
	httpGetResp *get(httpGetResp *r, const std::string url, const std::string ua, void *wc,
						const long timeout);
	CURLcode get(std::string url, std::string ua, void *wc);
	bool isOk(void);
private:
#define SETOPT(opt, par) curl_easy_setopt(this->curl, opt, par);
};

#endif /* __HTTP_H */
