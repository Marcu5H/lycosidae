#ifndef __UI_H
#define __UI_H 1

#include <termios.h>

//#define UI_CL_LINE

class UI {
private:
	struct termios term;
public:
	UI();
	//void uiClrLine();
	void flush();
	~UI();
};

#endif /* __UI_H */
