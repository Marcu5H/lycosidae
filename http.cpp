#include <iostream>
#include <string>
#include <cstdarg>
#include <curl/curl.h>

#include "http.hpp"

static bool curl_is_init = false;


HTTPClient::HTTPClient(void)
{
	if (!curl_is_init) {
		/* Globaly initialize libcurl with all internal sub modules in libcurl
		   enanbled */
		curl_global_init(CURL_GLOBAL_ALL);
		curl_is_init = true;
	}

	this->curl = curl_easy_init();
	if (!this->curl)
		curl_is_init = false;
}

bool
HTTPClient::isOk(void)
{
	return curl_is_init;
}

HTTPClient::httpGetResp *
HTTPClient::get(httpGetResp *r, const std::string url, const std::string ua,
				void *wc, const long timeout)
{
	SETOPT(CURLOPT_URL, url.c_str());
	if (ua.size() > 0) SETOPT(CURLOPT_USERAGENT, ua.c_str());
	SETOPT(CURLOPT_WRITEFUNCTION, wc);
	SETOPT(CURLOPT_WRITEDATA, &r->resp);
	SETOPT(CURLOPT_TIMEOUT, timeout);

	r->cc = curl_easy_perform(curl);

	curl_easy_getinfo(curl, CURLINFO_HTTP_CODE, &r->code);

	return r;
}

HTTPClient::~HTTPClient(void)
{
	if (curl_is_init) {
		curl_easy_cleanup(this->curl);
		curl_global_cleanup();
		curl_is_init = false;
	}
}
