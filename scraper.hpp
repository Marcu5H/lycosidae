#include <iostream>
#ifndef __SCRAPER_H
#define __SCRAPER_H 1

#include <vector>
#include <string>
#include <boost/regex.hpp>
#include <curl/curl.h>

#include "http.hpp"

struct scraped_s {
	std::string url;
	std::vector<std::string> scraped_urls;
	long http_response_code;
	CURLcode cc;
	long timeout;
};

/* Defined globaly for compatability issues with libcurl */
static std::vector<std::string> urls;
static std::vector<std::string> crawled_urls;

class Scraper {
private:
	bool httpOk;
	boost::regex urlRegex;
private:
	HTTPClient client;
	static size_t
	dataHandlerCallback(char *buffer, size_t size, size_t nitems, void *s)
	{
		((std::string*)s)->append((char*)buffer, 0, size*nitems);
		return size * nitems;
	}
public:
	Scraper(std::string init_url);
	bool isOk();
	struct scraped_s *scrape(struct scraped_s *s);
	void fetchURLs();
	long getURLCountInQue();
	long getURLCountCrawled();
};

#endif /* __SCRAPER_H */
