#include <iostream>
#include <termios.h>

#include "ui.hpp"

UI::UI()
{
	std::ios::sync_with_stdio(false);
	tcgetattr(fileno(stdin), &this->term);

    this->term.c_lflag &= ~ECHO;
    tcsetattr(fileno(stdin), 0, &this->term);
}

void
UI::flush()
{
	std::cout.flush();
}

UI::~UI()
{
    this->term.c_lflag |= ECHO;
    tcsetattr(fileno(stdin), 0, &this->term);
}
